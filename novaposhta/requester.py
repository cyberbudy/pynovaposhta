import logging
from typing import Optional, Dict, Type, List, Union

from httpx import Client, Response, Request, ConnectTimeout
from pydantic import BaseModel

from novaposhta.exceptions import InvalidDataError


class Requester:
    """
    HTTP handler
    """

    def __init__(self):
        self.default_headers = {'Content-Type': 'application/json'}

    def make_request(self, request: Request, retries: int = 5) -> Response:
        """
        Performs HTTP request

        :param request: Request instance to perform.
        :param retries: Max number of retries failed requests. Default: 5
        :return: HTTP response.
        """

        while retries > 0:
            try:
                return Client().send(request, timeout=3)
            except ConnectTimeout:
                retries -= 1
                continue

    def build_request(
        self,
        url: str,
        json: Dict,
        headers: Dict = {},
        method: str = 'POST',
        **kwargs
    ) -> Request:
        """
        Build request to perform.

        :param url: Request URL.
        :param json: JSON data to POST.
        :param headers: HTTP headers.
        :param method: Request method
        :param kwargs: Extra data. For more details check `httpx.Request`
        :return: Request instance.
        """

        headers = {
            **self.default_headers,
            **headers,
        }

        return Request(
            method=method,
            url=url,
            json=json,
            headers=headers,
            **kwargs
        )

    def process_response(
        self,
        response: Response,
        model: Type[BaseModel],
        key: Optional[str] = None,
        single: Optional[bool] = False,
        raise_for_status: bool = True
    ) -> Union[BaseModel, List[BaseModel]]:
        """
        Processes response.

        :param response: Response instance.
        :param model: Pydantic model to deserialize json data.
        :param key: Key where data is stored.
        :param single: Whether the response contains singular response or multiple.
        :param raise_for_status: Whether to raise error due to HTTP error status
        :return: Single item or array of items
        """

        if raise_for_status:
            response.raise_for_status()

        data = response.json()

        if data['errors']:
            raise InvalidDataError('Invalid addresses', data['errors'], data)

        if data['warnings']:
            logging.warning(data['warnings'])

        response_data = data['data']

        if key:
            response_data = response_data[0][key]

        data = [model(**x) for x in response_data]

        if single:
            return data[0]

        return data
