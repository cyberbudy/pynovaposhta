from typing import Dict, TYPE_CHECKING, Optional

from ..requester import Requester

if TYPE_CHECKING:  # pragma: no cover
    from novaposhta.client import AbstractNovaposhta


class AbstractClient:
    BASE_URL = 'https://api.novaposhta.ua/v2.0/json'
    MODEL = ''

    def __init__(
        self,
        client: 'AbstractNovaposhta',
        base_url: Optional[str] = None,
        requester: Optional[Requester] = None
    ):
        self.client = client
        self.base_url = (base_url or self.BASE_URL).rstrip('/')
        self.requester = requester or Requester()

    def build_url(self, method: str):
        return f'{self.base_url}/{self.MODEL}/{method}'

    def build_params(self, method: str, properties: Optional[Dict] = None):
        if not properties:
            properties = {}

        return {
            "apiKey": self.client.api_key,
            "modelName": self.MODEL,
            "calledMethod": method,
            "methodProperties": {
                key: value for key, value in properties.items() if value is not None
            }
        }
