# -*- coding: utf-8 -*-

# DO NOT EDIT THIS FILE!
# This file has been autogenerated by dephell <3
# https://github.com/dephell/dephell

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import os.path

readme = ''
here = os.path.abspath(os.path.dirname(__file__))
readme_path = os.path.join(here, 'README.rst')
if os.path.exists(readme_path):
    with open(readme_path, 'rb') as stream:
        readme = stream.read().decode('utf8')

setup(
    long_description=readme,
    name='pynovaposhta',
    version='0.2.0',
    description='Novaposhta client for humans',
    python_requires='>=3.6',
    project_urls={"repository": "https://gitlab.com/cyberbudy/pynovaposhta/"},
    author='cyberbudy',
    author_email='cyberbudy@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta', 'Programming Language :: Python :: 3',
        'Intended Audience :: Developers', 'Topic :: Utilities',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent'
    ],
    packages=['novaposhta', 'novaposhta.models', 'novaposhta.schemas'],
    package_dir={"": "."},
    package_data={},
    install_requires=[
        'httpx==0.*,>=0.16.1', 'pydantic==1.*,>=1.7.3', 'pyhumps==1.*,>=1.6.1'
    ],
    extras_require={
        "dev": [
            "coverage==5.*,>=5.3.1", "flake8==3.*,>=3.8.4",
            "ipython==7.*,>=7.19.0", "mypy==0.*,>=0.790.0",
            "pytest==6.*,>=6.2.1", "pytest-cov==2.*,>=2.10.1"
        ]
    },
)
