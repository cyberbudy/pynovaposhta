# Change log

## [0.2.0] - 2020-01-10

### Added

- Added support of counterparties model

## [0.1.0] - 2020-01-10

### Added

- Added support of addresses model
- First release
